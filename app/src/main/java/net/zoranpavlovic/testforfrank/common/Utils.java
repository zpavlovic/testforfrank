package net.zoranpavlovic.testforfrank.common;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Zoran on 21/06/2017.
 */

public class Utils {

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
