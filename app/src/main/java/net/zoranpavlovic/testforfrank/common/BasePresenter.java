package net.zoranpavlovic.testforfrank.common;

import android.view.View;

/**
 * Created by Zoran on 21/06/2017.
 */

public interface BasePresenter<T extends View> {

    void onViewDestroy();

}