package net.zoranpavlovic.testforfrank.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.zoranpavlovic.testforfrank.R;
import net.zoranpavlovic.testforfrank.common.Utils;
import net.zoranpavlovic.testforfrank.location.LocationController;
import net.zoranpavlovic.testforfrank.weather.models.Weather;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WeatherActivity extends AppCompatActivity implements WeatherView {

    @BindView(R.id.iv_weather) ImageView ivWeather;
    @BindView(R.id.et_zip) EditText etZip;
    @BindView(R.id.tv_temp) TextView tvTemp;
    @BindView(R.id.tv_high_temp) TextView tvHigh;
    @BindView(R.id.tv_low_temp) TextView tvLow;
    @BindView(R.id.tv_change_zip) TextView tvChangeZip;

    private WeatherPresenter presenter;

    private boolean isZipRequest = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        getWeatherForMyLocation();

        listenForZipChange();
    }

    private void listenForZipChange() {
        etZip.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if  ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    isZipRequest = true;
                    presenter.getWeatherByZip(etZip.getText().toString());
                }
                return true;
            }
        });
    }

    @OnClick(R.id.tv_change_zip)
    void changeZip() {
        presenter.getWeatherByZip(etZip.getText().toString());
    }

    @OnClick(R.id.tv_refresh)
    void refresh(){
        if(Utils.isConnected(this)){
            Toast.makeText(this, "Refreshing data...", Toast.LENGTH_SHORT).show();
            if(isZipRequest){
                presenter.getWeatherByZip(etZip.getText().toString());
            } else{
                getWeatherForMyLocation();
            }
        }
    }

    private void getWeatherForMyLocation() {
        presenter = new WeatherPresenterImpl(this, this);
        LocationController locationController = new LocationController(this);
        if(locationController.canGetLocation()){
            isZipRequest = false;
            presenter.getWeatherByLocation(locationController.getLatitude(), locationController.getLongitude());
        } else{
            locationController.showSettingsAlert();
        }
    }

    @Override
    public void onWeatherLoaded(Weather weather) {
        setTemperature(weather);
        setImageBasedOnWeather(weather);
    }

    private void setImageBasedOnWeather(Weather weather) {
        if(isWeatherType(weather, "Clear")){
            Picasso.with(getApplicationContext()).load(R.drawable.clear).into(ivWeather);
        } else if(isWeatherType(weather, "Clouds")){
            Picasso.with(getApplicationContext()).load(R.drawable.clouds).into(ivWeather);
        } else if(isWeatherType(weather, "Rain")){
            Picasso.with(getApplicationContext()).load(R.drawable.clouds).into(ivWeather);
        } else if(isWeatherType(weather, "Snow")){
            Picasso.with(getApplicationContext()).load(R.drawable.snow).into(ivWeather);
        } else if(isWeatherType(weather, "Drizzle")){
            Picasso.with(getApplicationContext()).load(R.drawable.drizzle).into(ivWeather);
        } else if(isWeatherType(weather, "Atmosphere")){
            Picasso.with(getApplicationContext()).load(R.drawable.atmosphere).into(ivWeather);
        }
    }

    private boolean isWeatherType(Weather weather, String type) {
        return weather.getWeather().get(0).getMain().equals(type);
    }

    private void setTemperature(Weather weather) {
        tvTemp.setText(String.valueOf(weather.getMain().getTemp())+"F");
        tvHigh.setText("High temp: "+String.valueOf(weather.getMain().getTempMax())+"F");
        tvLow.setText("Low temp: "+String.valueOf(weather.getMain().getTempMin())+"F");
    }

    @Override
    public void onWeatherError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
