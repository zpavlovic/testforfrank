package net.zoranpavlovic.testforfrank.weather;

import net.zoranpavlovic.testforfrank.weather.models.Weather;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Zoran on 21/06/2017.
 */

public interface WeatherService {

    @GET("weather?")
    Observable<Weather> getWeatherWithZip(@Query("zip") String zip, @Query("appid") String appId, @Query("units") String unit);

    @GET("weather?")
    Observable<Weather> getWeatherByLocation(@Query("lat") String lat, @Query("lon") String lon, @Query("appid") String appId, @Query("units") String unit);
}
