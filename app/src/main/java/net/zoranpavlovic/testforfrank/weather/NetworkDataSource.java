package net.zoranpavlovic.testforfrank.weather;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Zoran on 21/06/2017.
 */

public class NetworkDataSource {

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public static NetworkDataSource instance;
    private static Gson gson;
    private static Retrofit retrofit;
    private static OkHttpClient okHttpClient;

    public static synchronized NetworkDataSource getInstance(){
        if(instance == null){
            instance = new NetworkDataSource();
            gson = getGson();
        }
        return instance;
    }

    public static Retrofit getRetrofit(Context context){
        if(retrofit == null) {

            HttpLoggingInterceptor httpLoggingInterceptor = setUpHttpLogging();

            return retrofit = new Retrofit.Builder()
                    .client(getOkHttpClient(httpLoggingInterceptor, context))
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .baseUrl(BASE_URL)
                    .build();
        }
        return retrofit;
    }

    @NonNull
    private static OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Context context) {
        if(okHttpClient == null){
            okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .build();
        }
        return okHttpClient;
    }

    @NonNull
    private static HttpLoggingInterceptor setUpHttpLogging() {
        return new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.i("HTTP", message);
            }
        });
    }

    private static Gson getGson(){
        if(gson == null){
            gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        }
        return  gson;
    }
}
