package net.zoranpavlovic.testforfrank.weather;

import net.zoranpavlovic.testforfrank.weather.models.Weather;

/**
 * Created by Zoran on 21/06/2017.
 */

public interface WeatherView {

    void onWeatherLoaded(Weather weather);

    void onWeatherError(String error);
}
