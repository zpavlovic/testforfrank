package net.zoranpavlovic.testforfrank.weather;

import android.content.Context;
import android.widget.Toast;


import net.zoranpavlovic.testforfrank.common.Utils;
import net.zoranpavlovic.testforfrank.weather.models.Weather;

import retrofit2.Retrofit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Zoran on 21/06/2017.
 */

public class WeatherPresenterImpl implements WeatherPresenter {

    public static final String API_KEY = "fe4b6fea3651349a6c717287c181085b";
    public static final String UNITS = "imperial";

    private Retrofit retrofit;
    private Context context;
    private WeatherView view;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public WeatherPresenterImpl(Context context, WeatherView view) {
        this.context = context;
        this.view = view;
        retrofit = NetworkDataSource.getRetrofit(context);
    }

    @Override
    public void getWeatherByZip(String zip) {
        if(Utils.isConnected(context)) {
            compositeSubscription.add(retrofit.create(WeatherService.class).getWeatherWithZip(zip + ",us", API_KEY, UNITS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Weather>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.onWeatherError(e.getLocalizedMessage());
                        }

                        @Override
                        public void onNext(Weather weatherData) {
                            view.onWeatherLoaded(weatherData);
                        }
                    }));
        } else{
            Toast.makeText(context, "No Internet access!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getWeatherByLocation(double lat, double lon) {
        if (Utils.isConnected(context)) {
            compositeSubscription.add(retrofit.create(WeatherService.class).getWeatherByLocation(String.valueOf(lat), String.valueOf(lon), API_KEY, UNITS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Weather>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.onWeatherError(e.getLocalizedMessage());
                        }

                        @Override
                        public void onNext(Weather weatherData) {
                            view.onWeatherLoaded(weatherData);
                        }
                    }));
        } else{
            Toast.makeText(context, "No Internet access!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onViewDestroy() {
        this.view = null;
        if(compositeSubscription != null){
            compositeSubscription.clear();
        }
    }
}
