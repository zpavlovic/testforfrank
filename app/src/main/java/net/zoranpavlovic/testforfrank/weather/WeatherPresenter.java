package net.zoranpavlovic.testforfrank.weather;

import net.zoranpavlovic.testforfrank.common.BasePresenter;

/**
 * Created by Zoran on 21/06/2017.
 */

public interface WeatherPresenter extends BasePresenter{

    void getWeatherByZip(String zip);

    void getWeatherByLocation(double lat, double lon);
}
